package pgconn

import (
	"context"
	"database/sql"
)

// sqdSQLClient реализует обертку над стандартным клиентом sql.DB для библиотеки sqd.
type sqdSQLClient struct {
	MainClient *sql.DB
}

// StartSqdTransaction выполняет инициализацию транзакции и возвращает ее
func (s sqdSQLClient) StartSqdTransaction(ctx context.Context, options *sql.TxOptions) (*sql.Tx, error) {
	return s.MainClient.BeginTx(ctx, options)
}

// NewSqdClient реализует конструктор клиента
func NewSqdClient(client *sql.DB) SqdSQL {
	return &sqdSQLClient{
		MainClient: client,
	}
}
