package pgconn

import (
	"fmt"
	"github.com/jackc/pgx"
	"github.com/jackc/pgx/stdlib"
	"github.com/jmoiron/sqlx"
	"github.com/sirupsen/logrus"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"os"
	"runtime/debug"
	"strings"
)

// Connect выполняет подключение к базе данных PostgresSQL по переданным параметрам.
func Connect(
	loggerPrefix string,
	config ConnectionConfig,
) (connect *Connection, err error) {
	log := logrus.WithField("prefix", fmt.Sprintf("Connection [%s]", loggerPrefix))
	defer func() {
		err := recover()
		if nil != err {
			log.WithField("stack", string(debug.Stack())).Print(fmt.Sprintf(`Unrecoverable error: %v`, err))
			os.Exit(2)
		}
	}()

	log.Info(`Init connection`)

	d := &stdlib.DriverConfig{
		ConnConfig: pgx.ConnConfig{
			RuntimeParams: map[string]string{
				"standard_conforming_strings": "on",
			},
			PreferSimpleProtocol: false,
		},
	}
	stdlib.RegisterDriverConfig(d)

	connString := fmt.Sprintf(
		`host=%v port=%v user=%v password=%v dbname=%v sslmode=%v`,
		config.PgHost,
		config.PgPort,
		config.PgUser,
		config.PgPassword,
		config.PgDatabase,
		config.PgSslMode,
	)

	if 0 != len(strings.TrimSpace(config.PgSslRootCert)) {
		connString = connString + ` sslrootcert=` + config.PgSslRootCert
	}

	baseDB, err := sqlx.Connect(
		"pgx",
		d.ConnectionString(connString),
	)
	if err != nil {
		return nil, err
	}

	log.Info(`Connected to DB`)

	baseDB.SetMaxIdleConns(config.PgMaxIdleConn)
	baseDB.SetMaxOpenConns(config.PgMaxOpenConn)

	gormDB, err := gorm.Open(postgres.New(postgres.Config{
		Conn: baseDB.DB,
	}), &gorm.Config{
		SkipDefaultTransaction: true,
	})

	return &Connection{
		DB:      baseDB,
		GORM:    gormDB,
		SqdSQL:  NewSqdClient(baseDB.DB),
		SqdGORM: NewSqdGORMClient(gormDB),
		SqdSQLX: NewSqdSQLXClient(baseDB),
	}, nil
}
