package pgconn

// ConnectionConfig описывает параметры конфигурации подключения к PostgresSQL
type ConnectionConfig struct {
	PgHost        string
	PgPort        int
	PgUser        string
	PgPassword    string
	PgDatabase    string
	PgMaxIdleConn int
	PgMaxOpenConn int
	PgSslMode     string
	PgSslRootCert string
}
