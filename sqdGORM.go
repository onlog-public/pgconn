package pgconn

import (
	"context"
	"database/sql"
	"gorm.io/gorm"
)

// sqdGORMClient реализует клиент SQD для работы с транзакциями Gorm.
type sqdGORMClient struct {
	MainClient *gorm.DB
}

// StartSqdTransaction выполняет инициализацию транзакции и возвращает ее
func (s sqdGORMClient) StartSqdTransaction(
	_ context.Context,
	options *sql.TxOptions,
) (*GormTX, error) {
	tx := s.MainClient.Begin(options)

	return &GormTX{
		DB: tx,
	}, nil
}

// NewSqdGORMClient реализует конструктор клиента Gorm, подходящего для SQD.
func NewSqdGORMClient(client *gorm.DB) SqdGORM {
	return sqdGORMClient{
		MainClient: client,
	}
}
