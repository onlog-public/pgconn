package pgconn

import (
	"database/sql"
	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	"github.com/sirupsen/logrus"
)

// ApplyMigrations выполняет применение миграций по переданному
// пути к директории миграций и по номеру версии, до которой
// необходимо применить их.
func ApplyMigrations(
	db *sql.DB,
	path string,
	version int,
) error {
	log := logrus.WithField("prefix", "Migrations")

	log.Info(`Started migrations`)
	driver, _ := postgres.WithInstance(db, &postgres.Config{})
	m, err := migrate.NewWithDatabaseInstance(
		"file://"+path,
		"postgres",
		driver,
	)

	if nil != err {
		return err
	}

	log.Info(`Apply migrations`)

	if version != 0 {
		err = m.Steps(version)
	} else {
		err = m.Up()
	}

	if nil != err && err != migrate.ErrNoChange {
		return err
	}

	return nil
}
