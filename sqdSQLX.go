package pgconn

import (
	"context"
	"database/sql"
	"github.com/jmoiron/sqlx"
)

// sqdSQLClient реализует обертку над стандартным клиентом sql.DB для библиотеки sqd.
type sqdSQLXCClient struct {
	MainClient *sqlx.DB
}

// StartSqdTransaction выполняет инициализацию транзакции и возвращает ее
func (s sqdSQLXCClient) StartSqdTransaction(ctx context.Context, options *sql.TxOptions) (*sqlx.Tx, error) {
	return s.MainClient.BeginTxx(ctx, options)
}

// NewSqdSQLXClient реализует конструктор клиента
func NewSqdSQLXClient(client *sqlx.DB) SqdSQLX {
	return &sqdSQLXCClient{
		MainClient: client,
	}
}
