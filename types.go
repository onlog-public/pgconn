package pgconn

import (
	"database/sql"
	"github.com/jmoiron/sqlx"
	"gitlab.systems-fd.com/packages/golang/helpers/sqd"
	"gorm.io/gorm"
)

// GormTX реализует транзакцию Gorm, подходящую под пакет SQD.
// Переопределяет методы коммита и отката для того, чтобы они
// соответствовали интерфейсу sqd.Tx.
type GormTX struct {
	*gorm.DB
}

// Commit выполняет коммит транзакции
func (g *GormTX) Commit() error {
	return g.DB.Commit().Error
}

// Rollback выполняет откат транзакции
func (g *GormTX) Rollback() error {
	return g.DB.Rollback().Error
}

// SqdSQL описывает тип клиента для SQD с базовым типов
// подключения.
type SqdSQL sqd.SqlClient[*sql.Tx, *sql.TxOptions]

// SqdGORM описывает тип клиента для SQD с подключением
// с использованием ORM Gorm
type SqdGORM sqd.SqlClient[*GormTX, *sql.TxOptions]

// SqdSQLX описывает тип клиента для SQD с подключением
// с использованием драйвера SQLX
type SqdSQLX sqd.SqlClient[*sqlx.Tx, *sql.TxOptions]
