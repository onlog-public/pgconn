package pgconn

import (
	"github.com/jmoiron/sqlx"
	"gorm.io/gorm"
)

// Connection содержит драйверы для подключения к БД PostgresSQL.
// Драйверы подключения по факту используют единый коннект, но
// содержат разнообразные обертки для подключения.
type Connection struct {
	DB      *sqlx.DB
	GORM    *gorm.DB
	SqdSQL  SqdSQL
	SqdGORM SqdGORM
	SqdSQLX SqdSQLX
}
